/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef PIXELCONDITIONSSERVICES_IPIXELBYTESTREAMERRORSSVC_H
#define PIXELCONDITIONSSERVICES_IPIXELBYTESTREAMERRORSSVC_H


#include "PixelConditionsTools/IPixelByteStreamErrorsSvc.h"

#endif
